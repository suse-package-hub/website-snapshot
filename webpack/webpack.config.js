var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    'styles': './stylus/main.styl'
  },
  output: {
    path: path.dirname(__dirname) + '/assets/static/gen',
    filename: '[name].css'
  },
  devtool: '#cheap-module-source-map',
  resolve: {
    modules: ['node_modules',],
    extensions: ['.js', '.styl']
  },
  module: {
    rules: [
      { test: /\.styl$/,
        loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader!stylus-loader'}) },
      { test: /\.css$/,
        loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader'}) },
      { test: /\.(woff2?|ttf|eot|svg|png|jpe?g|gif)$/,
        loader: 'file-loader' }
    ]
  },
  plugins: [
    new ExtractTextPlugin({filename: 'styles.css', allChunks: true})
  ]
};
