SUSE Package Hub Web Site
=========================

NOTE: THIS IS ONLY A SNAPSHOT COPY OF THE CODE.
This code repository does not contain the complete history.

This is the code used for generating the web site at
<https://packagehub.suse.com>. 

This is a static site that utilizes [Lektor](https://www.getlektor.com/)
for content management and site generation.

The site uses [TukTuk css framework](http://tuktuk.tapquo.com/)
and uses [Tipue Search](http://www.tipue.com/search/).

A simple python script is used to generate the packagelist based on
the repo meta data from download.opensuse.org. The script also generates
a simple page for each package as well as the Tipue search index.

Structure
---------
The layout of this source tree follows the Lektor structure:

**assets/**: fixed content used in the published site

**[content/](https://www.getlektor.com/docs/content/):** the site content in Lektor files

**[databags/](https://www.getlektor.com/docs/content/databags/):** used for the top menu

**[flowblocks/](https://www.getlektor.com/docs/models/flow/):** defines reusable site structure

**[models/](https://www.getlektor.com/docs/models/):** site structure and logic

**scripts/:** custom scripts to automate some of the data generation

**[templates/](https://www.getlektor.com/docs/templates/):** html templating

**PackageHub.lektorproject':** Lektor project configuration


Script Generated Content
------------------------
The update_package_list.py script will create raw Lektor content under
`content/packages/`. As well as the tipue search index in `assets/tipuesearch`


