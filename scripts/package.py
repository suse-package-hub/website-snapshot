#! /usr/bin/env/python
# -*- coding: utf-8 -*-
"""
    This module is used for generating the package database content
    on packagehub.suse.com

    :copyright: Copyright (c) 2016-2019 Scott Bahling, SUSE Software Solutions GmbH
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details
"""


import os
import re
import cgi
from collections import namedtuple
from datetime import datetime

PLATFORMS = set(['x86-64', 'AArch64', 'ppc64le', 's390x'])
CONTENTPATH = os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..', 'content'))
ASSETSPATH = os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..', 'assets'))
SKIP_CATEGORIES = ['other']
CATEGORY_PACKAGES = {}

Category = namedtuple('Category', 'id, name')
_slug_re = re.compile(r'([a-zA-Z0-9.-_]+)')

ChangeLogEntry = namedtuple('ChangeLogEntry', 'author, date, text')


# taken from lektor source except we replace '.' with 'dot'
def slugify(value):
    # XXX: not good enough
    value_ascii = value.strip().encode('ascii', 'ignore').strip().decode()
    rv = u' '.join(value_ascii.split()).lower()
    words = _slug_re.findall(rv)
    return '-'.join(words).replace('.', '-dot-')


def alphanum(string):
    def convert(text):
        return float(text) if text.isdigit() else text
    return [convert(c) for c in re.split('([0-9]+)', string)]


class PackageIndex(object):
    def __init__(self):
        self.index = {}
        self.bqueue = {}

    def add_package(self, pkg):
        if pkg.name.startswith('rpmlint'):
            return
        if pkg.arch == 'src':
            bpkg = None
            for bpkg in self.bqueue.get(pkg.name, {}).get(pkg.vr, []):
                pkg.archs.add(bpkg.arch)
                pkg.subpackages.add(bpkg.name)
                pkg.binaries.append(bpkg)
            if bpkg:
                self.bqueue[pkg.name][pkg.vr]

            self.index.setdefault(pkg.name, {})[pkg.vr] = pkg
            return

        base_pkg = self.get_package_release(pkg.base, pkg.basevr)
        if base_pkg is None:
            self.bqueue.setdefault(pkg.base, {}).setdefault(pkg.basevr, []).append(pkg)

        else:
            base_pkg.archs.add(pkg.arch)
            base_pkg.subpackages.add(pkg.name)
            base_pkg.binaries.append(pkg)

    def get_package_release(self, pkgname, pkgver):
        return self.index.get(pkgname, {}).get(pkgver, None)

    def get_package_releases(self, pkgname):
        versions = self.index.get(pkgname, {}).keys()
        if not versions:
            return []
        versions = sorted(versions, key=alphanum)

        return [self.index[pkgname][v] for v in versions]

    def get_latest_release(self, pkgname):
        return self.get_package_releases(pkgname)[-1]

    @property
    def base_packages(self):
        pkgnames = sorted(self.index.keys())
        return [self.get_latest_release(pname) for pname in pkgnames]

    def items(self):
        for pkg in self.base_packages:
            yield (pkg.name, self.get_package_releases(pkg.name))


class Package(object):

    arch_to_platform = {'x86_64': 'x86-64',
                        'aarch64': 'AArch64',
                        'aarch64_ilp32': 'AArch64',
                        }

    def __init__(self, data, nspace, repo='SLE-12'):
        self.root = data
        self.nspace = nspace
        self.repo = repo
        self.rpmns = '{%s}' % self.root.nsmap.get('rpm', None)
        self.binaries = []
        self.archs = set()
        self.subpackages = set()
        self.updateinfo = None
        self.latest_release = self.issued

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __lt__(self, other):
        return self.name < other.name

    @property
    def rpm_details(self):
        return self.root.find(self.nspace + 'format')

    @property
    def name(self):
        return self.root.find(self.nspace + 'name').text

    @property
    def version(self):
        return self.root.find(self.nspace + 'version').get('ver', None)

    @property
    def release(self):
        return self.root.find(self.nspace + 'version').get('rel', None)

    @property
    def arch(self):
        return self.root.find(self.nspace + 'arch').text

    @property
    def summary(self):
        return self.root.find(self.nspace + 'summary').text

    @property
    def description(self):
        description = self.root.find(self.nspace + 'description').text
        if description is None:
            return ''

        return cgi.escape(self.root.find(self.nspace + 'description').text)

    @property
    def url(self):
        return self.root.find(self.nspace + 'url').text

    @property
    def license(self):
        return self.rpm_details.find(self.rpmns + 'license').text

    @property
    def licenses(self):
        return re.split(r' or | and ', self.license)

    @property
    def sourcerpm(self):
        return self.rpm_details.find(self.rpmns + 'sourcerpm').text

    @property
    def build_time(self):
        tstamp = self.root.find(self.nspace + 'time').get('build', None)
        if tstamp is None:
            return None
        dt = datetime.fromtimestamp(int(tstamp))
        return dt

    @property
    def issued(self):
        if self.updateinfo is not None:
            return self.updateinfo.issued_date
        else:
            return self.build_time

    @property
    def rpmgroup(self):
        return self.rpm_details.find(self.rpmns + 'group').text

    @property
    def categories_from_rpmgroup(self):
        if '/' in self.rpmgroup:
            return [Category(id=slugify(c), name=c) for c in self.rpmgroup.split('/')
                    if c not in SKIP_CATEGORIES]
        if self.rpmgroup not in SKIP_CATEGORIES:
            return [Category(id=slugify(self.rpmgroup), name=self.rpmgroup)]

        return []

    @property
    def is_debug(self):
        if '-debuginfo' in self.name or '-debugsource' in self.name:
            return True

        return False

    @property
    def vr(self):
        return '%s-%s' % (self.version, self.release)

    @property
    def nvr(self):
        return '%s-%s-%s' % (self.name, self.version, self.release)

    @property
    def base(self):
        if not self.sourcerpm:
            return self.name
        return '-'.join(self.sourcerpm.split('-')[:-2])

    @property
    def basevr(self):
        if not self.sourcerpm:
            return self.vr
        vr = '-'.join(self.sourcerpm.split('-')[-2:])
        return '.'.join(vr.split('.')[:-2])

    @property
    def binarchs(self):
        return [x for x in self.archs if x not in ['src', 'nosrc']]

    @property
    def platforms(self):
        if self.binarchs == ['noarch']:
            return PLATFORMS
        platforms = [self.arch_to_platform.get(x, x) for x in self.binarchs]
        return PLATFORMS.intersection(set(platforms))

    @property
    def updateid(self):
        if self.updateinfo is not None:
            return self.updateinfo.id
        return None

    def dependency(self, dtype):
        this_dependency = self.rpm_details.find(self.rpmns + dtype)
        if this_dependency is None:
            return []
        entries = this_dependency.findall(self.rpmns + 'entry')
        if entries is None:
            return []
        return [Dependency(p) for p in entries]

    @property
    def provides(self):
        return self.dependency('provides')

    @property
    def requires(self):
        return self.dependency('requires')


class Dependency(object):

    flag_map = {'EQ': '==',
                'GT': '>',
                'LT': '<',
                'GE': '>=',
                'LE': '<=',
                }

    def __init__(self, data):
        self.root = data

    def __repr__(self):
        flags = self.flag_map.get(self.flags, self.flags) or ''
        return '{} {} {}'.format(self.name, flags, self.full_ver).strip()

    def __str__(self):
        return self.__repr__()

    @property
    def full_ver(self):
        e = self.epoch or ''
        if e == '0':
            e = ''
        v = self.ver or ''
        r = self.rel or ''
        if v and r:
            v = '%s-%s' % (v, r)
        return '{} {}'.format(e, v).strip()

    @property
    def name(self):
        return self.root.get('name', 'no-name')

    @property
    def flags(self):
        return self.root.get('flags', None)

    @property
    def epoch(self):
        return self.root.get('epoch', None)

    @property
    def ver(self):
        return self.root.get('ver', None)

    @property
    def rel(self):
        return self.root.get('rel', None)
