# -*- coding: utf-8 -*-
#
"""
    This module is used for generating the package database content
    on packagehub.suse.com

    :copyright: Copyright (c) 2016-2019 Scott Bahling, SUSE Software Solutions GmbH
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details
"""

import os
import requests
import zlib
from lxml import etree
from package import Package

# Disable xml tree limits as we have some large XML documents
# to parse. We don't have a DoS risk.
# http://lxml.de/FAQ.html#is-lxml-vulnerable-to-xml-bombs
hugeparser = etree.XMLParser(huge_tree=True)


def dummy_xml():
    return etree.fromstring('<None/>')


class Channel(object):
    def __init__(self, name, url, channeltype='product', logger=None):
        self.name = name
        self.url = url
        self.whitelist = None
        self.logger = logger
        self.type = channeltype

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __hash__(self):
        return hash(self.url)

    def __eq__(self, other):
        return self.url == other.url

    def __lt__(self, other):
        return self.name < other.name

    def filename(self, ext=''):
        return '%s-%s%s' % (self.name, self.type, ext)

    def dump_filename(self):
        return self.filename('.dump')

    def channel_filename(self):
        return self.filename('.channel')

    def pickle_filename(self):
        return self.filename('.p')


class RepoData(Channel):

    def get_repodata_db(self, db):
        repodata = self.repomd
        namespace = '{%s}' % repodata.nsmap.get(None, None)

        find = etree.ETXPath(f'{namespace}data[@type="{db}"]/{namespace}location')

        if not find(repodata):
            return dummy_xml()

        url = find(repodata)[0].get('href', None)
        if url is not None:
            db_url = os.path.join(self.url, url)
        else:
            raise Exception('%s url not found!' % db)

        try:
            r = requests.get(db_url)
        except Exception as e:
            if self.logger:
                self.logger.error(e)
                self.logger.error('Failed to retrieve %s' % db_url)
            return None

        if not r.status_code == requests.codes.ok:
            if self.logger:
                self.logger.error('Failed to retrieve %s' % r.url)
            return None

        d = zlib.decompressobj(16 + zlib.MAX_WBITS)
        data = []
        for chunk in r.iter_content(1024):
            data.append(d.decompress(chunk))

        xml = b''.join(data)

        return etree.fromstring(xml, parser=hugeparser)

    @property
    def primary(self):
        try:
            return self._primary
        except AttributeError:
            self._primary = self.get_repodata_db('primary') or dummy_xml()
            return self._primary

    @property
    def filelists(self):
        try:
            return self._filelists
        except AttributeError:
            self._filelists = self.get_repodata_db('filelists') or dummy_xml()
            return self._filelists

    @property
    def other(self):
        try:
            return self._other
        except AttributeError:
            self._other = self.get_repodata_db('other') or dummy_xml()
            return self._other

    @property
    def updateinfo(self):
        try:
            return self._updateinfo
        except AttributeError:
            self._updateinfo = self.get_repodata_db('updateinfo') or dummy_xml()
            return self._updateinfo

    @property
    def repomd(self):
        url = self.url + '/repodata/repomd.xml'
        try:
            r = requests.get(url)
            r.raise_for_status()
        except Exception as e:
            if self.logger:
                self.logger.error(e)
                self.logger.error('Failed to retrieve %s' % url)
                self.logger.error('Returning empty data')
            return dummy_xml()

        xml = b'\n'.join(r.content.splitlines()[1:])
        try:
            return etree.fromstring(xml, parser=hugeparser)
        except Exception as e:
            self.logger.error(xml)
            self.logger.error(e)
            exit(1)

    @property
    def packages(self):
        namespace = '{%s}' % self.primary.nsmap.get(None, None)
        return [Package(p, namespace, self) for p in self.primary.findall(namespace + 'package')]
