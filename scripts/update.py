#! /usr/bin/env/python
# -*- coding: utf-8 -*-
"""
    This module is used for generating the package database content
    on packagehub.suse.com

    :copyright: Copyright (c) 2016-2019 Scott Bahling, SUSE Software Solutions GmbH
        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License version 2 as
        published by the Free Software Foundation.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program (see the file COPYING); if not, write to the
        Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
    :license: GPL-2.0, see COPYING for details
"""

from datetime import datetime


class UpdatePackage(object):
    def __init__(self, data):
        self.root = data

    @property
    def name(self):
        return self.root.get('name')

    @property
    def version(self):
        return self.root.get('version')

    @property
    def release(self):
        return self.root.get('release')

    @property
    def arch(self):
        return self.root.get('arch')


class UpdateReference(object):
    pretty = {'bugzilla': 'Bugzilla',
              'cve': 'CVE',
              'fate': 'FATE',
              }

    def __init__(self, data):
        self.root = data

    def __lt__(self, other):
        return self.id < other.id

    @property
    def href(self):
        return self.root.get('href')

    @property
    def id(self):
        return self.root.get('id')

    @property
    def title(self):
        return self.root.get('title')

    @property
    def type(self):
        return self.root.get('type')

    @property
    def pretty_type(self):
        return self.pretty.get(self.type, self.type)


class Update(object):

    def __init__(self, data):
        self.root = data

    @property
    def id(self):
        return self.root.find('id').text

    @property
    def title(self):
        return self.root.find('title').text

    @property
    def type(self):
        return self.root.get('type')

    @property
    def description(self):
        return self.root.find('description').text

    @property
    def references(self):
        refs = self.root.find('references')
        r = [UpdateReference(r) for r in refs.findall('reference')]
        return r

    @property
    def severity(self):
        return self.root.find('severity').text

    @property
    def issued_date(self):
        ts = int(self.root.find('issued').get('date'))
        return datetime.fromtimestamp(ts)

    @property
    def pkglist(self):
        pkgs = []
        for pkg in self.root.findall('pkglist/collection/package'):
            pkgs.append(UpdatePackage(pkg))

        return pkgs

    @property
    def base_pkgs(self):
        return [pkg for pkg in self.pkglist if pkg.arch == 'src']
